#pragma once
#include <vector>
#include "Scene.h"
#include "Agent.h"

class SceneCustomScene :
	public Scene
{
public:
	SceneCustomScene();
	~SceneCustomScene();
	void update(float dtime, SDL_Event *event);
	void draw();
	const char* getTitle();
private:
	std::vector<Agent*> agents;
	std::vector<Agent*> entities;
	Vector2D target;
};

