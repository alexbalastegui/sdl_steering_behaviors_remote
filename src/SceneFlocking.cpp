#include "SceneFlocking.h"

SceneFlocking::SceneFlocking() {
	for (int i = 0; i < 15; ++i) {
		Agent *agent = new Agent;
		agent->setPosition(Vector2D(randomBetween(720,480), randomBetween(480,240)));
		//agent->loadSpriteTexture("../res/soldier.png", 4);
		agents.push_back(agent);

	}
	target = Vector2D(640, 360);
}

SceneFlocking::~SceneFlocking() {
	for (int i = 0; i < (int)agents.size(); i++)
	{
		delete agents[i];
	}
}

void SceneFlocking::update(float dtime, SDL_Event *event) {
	switch (event->type) {
	case SDL_MOUSEMOTION:
	case SDL_MOUSEBUTTONDOWN:
		if (event->button.button == SDL_BUTTON_LEFT)
		{
			for (int i = 0; i < agents.size(); ++i) {
				target = Vector2D((float)(event->button.x), (float)(event->button.y));
				agents[i]->setTarget(target);
			}
		}
		break;
	default:
		break;
	}
	
	for (int i = 0; i < agents.size(); ++i) {
		Vector2D separationDirection = agents[i]->Behavior()->FlocSeparation(agents, agents[i],100);
		Vector2D cohesionForce = agents[i]->Behavior()->FlocCohesion(agents, agents[i], 100);
		Vector2D alignmentForce = agents[i]->Behavior()->FlocAlignment(agents, agents[i], 100);

		Vector2D flockingForce = separationDirection * 1.35 + cohesionForce * 1 + alignmentForce * 0.4;
		flockingForce *= 150;
		Vector2D steeringForce = flockingForce +agents[i]->Behavior()->Seek(agents[i], agents[i]->getTarget(), 1, dtime)*2;
		
		agents[i]->update(steeringForce, dtime, event);
	}
	
}

void SceneFlocking::draw()
{
	draw_circle(TheApp::Instance()->getRenderer(), (int)target.x, (int)target.y, 15, 255, 0, 0, 255);
	for (int i = 0; i < agents.size(); ++i) {
		agents[i]->draw();
	}
}

const char* SceneFlocking::getTitle()
{
	return "SDL Steering Behaviors :: Flocking";
}