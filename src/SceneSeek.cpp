#include "SceneSeek.h"

SceneSeek::SceneSeek() {
	Agent *agent = new Agent;
	agent->setPosition(Vector2D(640, 360));
	agent->setTarget(Vector2D(640, 360));
	//agent->loadSpriteTexture("../res/soldier.png", 4);
	agents.push_back(agent);
	target = Vector2D(640, 360);

//	Agent *target = new Agent;
//	target->setPosition(Vector2D(200, 200));
//	target->setTarget(Vector2D(640, 360));
////	target->loadSpriteTexture("../res/soldier.png", 4);
//	agents.push_back(target);
}

SceneSeek::~SceneSeek() {
	for (int i = 0; i < (int)agents.size(); i++)
	{
		delete agents[i];
	}
}

void SceneSeek::update(float dtime, SDL_Event *event) {
	switch (event->type) {
	case SDL_MOUSEMOTION:
	case SDL_MOUSEBUTTONDOWN:
		if (event->button.button == SDL_BUTTON_LEFT)
		{
			target = Vector2D((float)(event->button.x), (float)(event->button.y));
			agents[0]->setTarget(target);
			//agents[1]->setTarget(target);
		}
		break;
	default:
		break;
	}
	Vector2D SteeringForce = agents[0]->Behavior()->Seek(agents[0],agents[0]->getTarget(),1,dtime);
	//Vector2D SteeringForce2 = agents[1]->Behavior()->Evade(agents[1], agents[0], dtime);
	agents[0]->update(SteeringForce, dtime, event);
	//agents[1]->update(SteeringForce2, dtime, event);
}

void SceneSeek::draw()
{
	draw_circle(TheApp::Instance()->getRenderer(), (int)target.x, (int)target.y, 15, 255, 0, 0, 255);
	agents[0]->draw();
	//agents[1]->draw();
}

const char* SceneSeek::getTitle()
{
	return "SDL Steering Behaviors :: Seek";
}