#include "SceneWander.h"

SceneWander::SceneWander() {
	Agent *agent = new Agent;
	agent->setPosition(Vector2D(640, 360));
	agent->setTarget(Vector2D(640, 360));
	//agent->loadSpriteTexture("../res/soldier.png", 4);
	agents.push_back(agent);

	Agent *agentB = new Agent;
	agentB->setPosition(Vector2D(600, 360));
	agentB->setTarget(Vector2D(600, 360));
	//agent->loadSpriteTexture("../res/soldier.png", 4);
	agentB->setColor(0, 156, 255, 255);
	agents.push_back(agentB);

	target = Vector2D(640, 360);


	//	Agent *target = new Agent;
	//	target->setPosition(Vector2D(200, 200));
	//	target->setTarget(Vector2D(640, 360));
	////	target->loadSpriteTexture("../res/soldier.png", 4);
	//	agents.push_back(target);
}

SceneWander::~SceneWander() {
	for (int i = 0; i < (int)agents.size(); i++)
	{
		delete agents[i];
	}
}

void SceneWander::update(float dtime, SDL_Event *event) {
	switch (event->type) {
	case SDL_MOUSEMOTION:
	case SDL_MOUSEBUTTONDOWN:
		if (event->button.button == SDL_BUTTON_LEFT)
		{
			target = Vector2D((float)(event->button.x), (float)(event->button.y));
			for(int i=0; i<agents.size(); ++i)
				agents[i]->setTarget(target);
			//agents[1]->setTarget(target);
		}
		break;
	default:
		break;
	}
	Vector2D SteeringForce = agents[0]->Behavior()->Wander(agents[0], 200, 100, 30, dtime, target)*150;
	Vector2D SteeringForce2 = agents[1]->Behavior()->OwnWander(agents[1], 200, 100, 30, 10, dtime, target);
	agents[0]->update(SteeringForce, dtime, event);
	agents[1]->update(SteeringForce2, dtime, event);
}

void SceneWander::draw()
{
	draw_circle(TheApp::Instance()->getRenderer(), (int)target.x, (int)target.y, 15, 255, 0, 0, 255);
	agents[0]->draw();
	agents[1]->draw();
}

const char* SceneWander::getTitle()
{
	return "SDL Steering Behaviors :: Wander";
}