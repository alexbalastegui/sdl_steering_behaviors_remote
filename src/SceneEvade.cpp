#include "SceneEvade.h"

SceneEvade::SceneEvade() {
	Agent *agent = new Agent;
	agent->setPosition(Vector2D(640, 200));
	agent->setTarget(Vector2D(640, 360));
	//agent->loadSpriteTexture("../res/soldier.png", 4);
	agents.push_back(agent);
	target = Vector2D(640, 360);

	Agent *target = new Agent;
	target->setPosition(Vector2D(640, 360));
	target->setTarget(Vector2D(640, 360));
	////	target->loadSpriteTexture("../res/soldier.png", 4);
	agents.push_back(target);
}

SceneEvade::~SceneEvade() {
	for (int i = 0; i < (int)agents.size(); i++)
	{
		delete agents[i];
	}
}

void SceneEvade::update(float dtime, SDL_Event *event) {
	switch (event->type) {
	case SDL_MOUSEMOTION:
	case SDL_MOUSEBUTTONDOWN:
		if (event->button.button == SDL_BUTTON_LEFT)
		{
			target = Vector2D((float)(event->button.x), (float)(event->button.y));
			agents[0]->setTarget(target);
			//agents[1]->setTarget(target);
		}
		break;
	default:
		break;
	}
	Vector2D SteeringForce = agents[0]->Behavior()->Seek(agents[0], agents[0]->getTarget(), 1, dtime);
	Vector2D SteeringForce2 = agents[1]->Behavior()->Evade(agents[1], agents[0], 0.3, dtime);
	agents[0]->update(SteeringForce, dtime, event);
	agents[1]->update(SteeringForce2, dtime, event);
}

void SceneEvade::draw()
{
	draw_circle(TheApp::Instance()->getRenderer(), (int)target.x, (int)target.y, 15, 255, 0, 0, 255);
	agents[0]->draw();
	agents[1]->draw();
}

const char* SceneEvade::getTitle()
{
	return "SDL Steering Behaviors :: Evade";
}