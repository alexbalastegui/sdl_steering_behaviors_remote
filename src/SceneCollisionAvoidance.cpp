#include "SceneCollisionAvoidance.h"

SceneCollisionAvoidance::SceneCollisionAvoidance() {
	
		agent = new Agent;
		agent->setPosition(Vector2D(randomBetween(640, 480), randomBetween(480, 240)));
		agent->setTarget(Vector2D(640, 360));
	

	for (int i = 0; i < 5; ++i) {
		Agent *entity = new Agent;
		entity->setPosition(Vector2D(randomBetween(SDL_SimpleApp::Instance()->getWinSize().x, 0), randomBetween(SDL_SimpleApp::Instance()->getWinSize().y, 0)));
		entity->setColor(255, 255, 0, 255);
		entities.push_back(entity);

	}
}

SceneCollisionAvoidance::~SceneCollisionAvoidance() {
		delete agent;
	for (int i = 0; i < (int)entities.size(); i++)
	{
		delete entities[i];
	}
}

void SceneCollisionAvoidance::update(float dtime, SDL_Event *event) {
	switch (event->type) {
	case SDL_MOUSEMOTION:
	case SDL_MOUSEBUTTONDOWN:
		if (event->button.button == SDL_BUTTON_LEFT)
		{
			target = Vector2D((float)(event->button.x), (float)(event->button.y));
				agent->setTarget(target);

		}
		break;
	default:
		break;
	}

		Vector2D SteeringForce = agent->Behavior()->Seek(agent, agent->getTarget(), 1, dtime)*1.5;

		SteeringForce += agent->Behavior()->CollsionAvoidance(agent, entities, 40, 60, dtime) * 5;


		agent->update(SteeringForce, dtime, event);
	
	//agents[1]->update(SteeringForce2, dtime, event);
}

void SceneCollisionAvoidance::draw()
{
	draw_circle(TheApp::Instance()->getRenderer(), (int)target.x, (int)target.y, 15, 255, 0, 0, 255);

	agent->draw();
	
	for (int i = 0; i < entities.size(); ++i) {
		entities.at(i)->draw(7);
	}
}

const char* SceneCollisionAvoidance::getTitle()
{
	return "SDL Steering Behaviors :: CollisionAvoidance";
}