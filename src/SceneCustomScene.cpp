#include "SceneCustomScene.h"

SceneCustomScene::SceneCustomScene() {

	for (int i = 0; i < 10; ++i) {
		Agent *agent = new Agent;
		agent->setPosition(Vector2D(randomBetween(640, 480), randomBetween(480, 240)));
		agent->setTarget(Vector2D(640, 360));
		agents.push_back(agent);
	}

	for (int i = 0; i < 5; ++i) {
		Agent *entity = new Agent;
		entity->setPosition(Vector2D(randomBetween(SDL_SimpleApp::Instance()->getWinSize().x, 0), randomBetween(SDL_SimpleApp::Instance()->getWinSize().y, 0)));
		entity->setColor(255, 255, 0, 255);
		entities.push_back(entity);

	}
}

SceneCustomScene::~SceneCustomScene() {
	for (int i = 0; i < (int)agents.size(); i++)
	{
		delete agents[i];
	}
	for (int i = 0; i < (int)entities.size(); i++)
	{
		delete entities[i];
	}
}

void SceneCustomScene::update(float dtime, SDL_Event *event) {
	switch (event->type) {
	case SDL_MOUSEMOTION:
	case SDL_MOUSEBUTTONDOWN:
		if (event->button.button == SDL_BUTTON_LEFT)
		{
			target = Vector2D((float)(event->button.x), (float)(event->button.y));
			for (int i = 0; i < agents.size(); ++i) {
				agents[i]->setTarget(target);
			}

		}
		break;
	default:
		break;
	}

	for (int i = 0; i < agents.size(); ++i) {
		Vector2D SteeringForce = agents[i]->Behavior()->Seek(agents[i], agents[i]->getTarget(), 1, dtime)*1.5;

		Vector2D separationForce, cohesionForce, alignmentForce;
		separationForce = agents[i]->Behavior()->FlocSeparation(agents, agents.at(i), 200);
		cohesionForce = agents[i]->Behavior()->FlocCohesion(agents, agents.at(i), 200);
		alignmentForce = agents[i]->Behavior()->FlocAlignment(agents, agents.at(i), 200);

		SteeringForce += (separationForce * 1 + cohesionForce * 0.5 + alignmentForce * 0.2) * 100;

		SteeringForce += agents[i]->Behavior()->CollsionAvoidance(agents[i], entities, 40, 60, dtime) * 5;

		SteeringForce += agents.at(i)->Behavior()->PerimeterAvoidance(agents.at(i), 50, SDL_SimpleApp::Instance()->getWinSize().x, SDL_SimpleApp::Instance()->getWinSize().y);

		agents[i]->update(SteeringForce, dtime, event);
	}
	//agents[1]->update(SteeringForce2, dtime, event);
}

void SceneCustomScene::draw()
{
	draw_circle(TheApp::Instance()->getRenderer(), (int)target.x, (int)target.y, 15, 255, 0, 0, 255);
	for (int i = 0; i < agents.size(); ++i) {
		agents.at(i)->draw();
	}

	for (int i = 0; i < entities.size(); ++i) {
		entities.at(i)->draw(7);
	}
}

const char* SceneCustomScene::getTitle()
{
	return "SDL Steering Behaviors :: CustomScene";
}