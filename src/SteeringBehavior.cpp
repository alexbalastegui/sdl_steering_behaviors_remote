#include "SteeringBehavior.h"



SteeringBehavior::SteeringBehavior()
{
}


SteeringBehavior::~SteeringBehavior()
{
}

Vector2D SteeringBehavior::KinematicSeek(Agent *agent, Vector2D target, float dtime)
{
	Vector2D steering = target - agent->position;
	steering.Normalize();
	return steering * agent->max_velocity;
}

Vector2D SteeringBehavior::KinematicSeek(Agent *agent, Agent *target, float dtime)
{
	return KinematicSeek(agent, target->position, dtime);
}

Vector2D SteeringBehavior::KinematicFlee(Agent *agent, Vector2D target, float dtime)
{
	Vector2D steering = agent->position - target;
	steering.Normalize();
	return steering * agent->max_velocity;
}

Vector2D SteeringBehavior::KinematicFlee(Agent *agent, Agent *target, float dtime)
{
	return KinematicFlee(agent, target->position, dtime);
}

/* Add here your own Steering Behavior functions definitions */

Vector2D SteeringBehavior::Seek(Agent *agent, Vector2D target, float factor, float dtime)
{
	Vector2D DesiredVelocity = target - agent->position;
	DesiredVelocity.Normalize();
	DesiredVelocity *= agent->max_velocity * factor;
	Vector2D SteeringForce = (DesiredVelocity - agent->velocity);
	SteeringForce /= agent->max_velocity;

	return SteeringForce * agent->max_force;
}

Vector2D SteeringBehavior::Seek(Agent *agent, Agent *target, float factor, float dtime)
{
	return Seek(agent, target->position, factor, dtime);
}

Vector2D SteeringBehavior::Flee(Agent *agent, Vector2D target,float factor, float dtime)
{
	Vector2D DesiredVelocity = agent->position - target;
	DesiredVelocity.Normalize();
	DesiredVelocity *= agent->max_velocity * factor;
	Vector2D SteeringForce = (DesiredVelocity - agent->velocity);
	SteeringForce /= agent->max_velocity;

	return SteeringForce * agent->max_force;
}

Vector2D SteeringBehavior::Flee(Agent *agent, Agent *target,float factor, float dtime)
{
	return Flee(agent, target->position, factor, dtime);
}

Vector2D SteeringBehavior::Arrive(Agent *agent, Vector2D target, float slowingRadius, float dtime)
{
	if ((target - agent->position).Length() > slowingRadius) {
		return Seek(agent, target,1, dtime);
	}
	else {
		return Seek(agent, target, (target - agent->position).Length()/slowingRadius, dtime);
	}
	
}

Vector2D SteeringBehavior::Arrive(Agent *agent, Agent *target, float slowingRadius, float dtime)
{
	return Arrive(agent, target->position, slowingRadius, dtime);
}

Vector2D SteeringBehavior::Pursue(Agent *agent, Agent *target, float dtime) {
	float T = (target->position - agent->position).Length() / agent->max_velocity;
	Vector2D DesiredVelocity = (target->position + target->velocity * T) - agent->position;
	DesiredVelocity.Normalize();
	DesiredVelocity *= agent->max_velocity;
	Vector2D SteeringForce = (DesiredVelocity - agent->velocity);
	SteeringForce /= agent->max_velocity;

	return SteeringForce * agent->max_force;
}


Vector2D SteeringBehavior::Evade(Agent *agent, Agent *target, float factor, float dtime) {
	float T = (target->position - agent->position).Length() / agent->max_velocity;
	Vector2D DesiredVelocity =  agent->position - (target->position + target->velocity * T);
	DesiredVelocity.Normalize();
	DesiredVelocity *= agent->max_velocity;
	Vector2D SteeringForce = (DesiredVelocity - agent->velocity);
	SteeringForce /= agent->max_velocity;

	return SteeringForce * agent->max_force*factor;
}

Vector2D SteeringBehavior::Wander(Agent * agent, float wanderOffset, float wanderRadius, float wanderMaxChange, float dtime, Vector2D & target)
{
	static float wanderAngle = 0.f;

	static Vector2D wanderSpot{ agent->position.x + wanderOffset,
		agent->position.y + wanderOffset};


	float randomFactor = ((rand() % 200) - 100) / 10;
	wanderAngle += randomFactor * wanderMaxChange*dtime / 2;

	wanderSpot = { agent->velocity.Normalize().x*wanderOffset, agent->velocity.Normalize().y*wanderOffset };

	target = { wanderSpot.x ,
		wanderSpot.y };

	return Seek(agent, { wanderSpot.x + wanderRadius * cos(wanderAngle),
		wanderSpot.y + wanderRadius * sin(wanderAngle) }, 1, dtime);
}

Vector2D SteeringBehavior::OwnWander(Agent * agent, float wanderOffset, float wanderRadius, float wanderMaxChange, float wanderMaxChangeOffset, float dtime, Vector2D &target)
{
	static float wanderAngle = 0.f;
	static float angle = 0.f;

	Vector2D wanderSpot{ agent->position.x+wanderOffset*cos(angle), 
		agent->position.y + wanderOffset*sin(angle) };
	

	float randomFactor = ((rand() % 200) - 100) / 10;
	wanderAngle += randomFactor * wanderMaxChange*dtime/2;

	float randomFactorOffset = ((rand() % 200) - 100) / 10;
	angle += randomFactorOffset * wanderMaxChangeOffset*dtime / 2;

	//target = { wanderSpot.x + wanderRadius * cos(wanderAngle),
	//	wanderSpot.y + wanderRadius * sin(wanderAngle) };

	wanderSpot = { agent->position.x + wanderOffset * cos(angle),
		agent->position.y + wanderOffset * sin(angle) };

	return Seek(agent, { wanderSpot.x + wanderRadius * cos(wanderAngle),  
		wanderSpot.y + wanderRadius * sin(wanderAngle) }, 1, dtime);

}
Vector2D SteeringBehavior::FlocSeparation(std::vector<Agent*> agents, Agent * agent, float NEIGHBOR_RADIUS) {
	int neighborCount = 0;
	Vector2D separationVector = {};

	for each (Agent* a in agents)
	{
		if (a != agent) {
			if (Vector2D::Distance(agent->getPosition(), a->getPosition()) < NEIGHBOR_RADIUS)
			{
				separationVector += (agent->getPosition() - a->getPosition());
				neighborCount++;
			}
		}
	}

	separationVector /= neighborCount;
	return Vector2D::Normalize(separationVector);
}

Vector2D SteeringBehavior::FlocCohesion(std::vector<Agent*> agents, Agent * agent, float NEIGHBOR_RADIUS) {
	int neighborCount = 0;
	Vector2D averagePosition = {};

	for each (Agent* a in agents)
	{
		if (a != agent) {
			if (Vector2D::Distance(agent->getPosition(), a->getPosition()) < NEIGHBOR_RADIUS)
			{
				averagePosition += a->getPosition();
				neighborCount++;
			}
		}
	}

	averagePosition /= neighborCount;
	averagePosition -= agent->getPosition();
	return Vector2D::Normalize(averagePosition);
}

Vector2D SteeringBehavior::FlocAlignment(std::vector<Agent*> agents, Agent * agent, float NEIGHBOR_RADIUS) {
	int neighborCount = 0;
	Vector2D averageVelocity = {};

	for each (Agent* a in agents)
	{
		if (a != agent) {
			if (Vector2D::Distance(agent->getPosition(), a->getPosition()) < NEIGHBOR_RADIUS)
			{
				averageVelocity += a->getVelocity();
				neighborCount++;
			}
		}
	}

	averageVelocity /= neighborCount;
	averageVelocity -= agent->getPosition();
	return Vector2D::Normalize(averageVelocity);
}

Vector2D SteeringBehavior::PerimeterAvoidance(Agent * agent, float margin, float wWidht, float wHeight)
{
	Vector2D desiredVelocity;
	Vector2D steeringForce;

	if (agent->getPosition().x < margin)
		desiredVelocity.x = agent->getMaxVelocity();
	else if(agent->getPosition().x > wWidht - margin)
		desiredVelocity.x = -agent->getMaxVelocity();
	if (agent->getPosition().y < margin)
		desiredVelocity.y = agent->getMaxVelocity();
	else if(agent->getPosition().y > wHeight - margin)
		desiredVelocity.y = -agent->getMaxVelocity();
	if (desiredVelocity.Length() > 0.0f) {
		steeringForce = desiredVelocity - agent->getVelocity();
		steeringForce /= agent->getMaxVelocity();
		steeringForce *= 100;
	}

	return steeringForce;
}

Vector2D SteeringBehavior::CollsionAvoidance(Agent * agent, std::vector<Agent*> entities, float coneAngle, float coneLength, float dtime)
{
	float shortestDistance=-1;
	Agent* nearestTarget = nullptr;
	bool collisionDetected = false;

	for each (Agent* entity in entities)
	{
		if (Vector2DUtils::IsInsideCone(entity->getPosition(), agent->getPosition(), 
			agent->getPosition()+Vector2D::Normalize(agent->getVelocity())*coneLength, coneAngle)) {

			float currDistance = Vector2D::Distance(agent->getPosition(), entity->getPosition());

			if (shortestDistance == -1 || currDistance < shortestDistance) {
				shortestDistance = currDistance;
				nearestTarget = entity;
				collisionDetected = true;
			}
		}
	}

	if (collisionDetected)
		return Flee(agent, nearestTarget, 0.51, dtime);

	return Vector2D(0, 0);
}

