#pragma once
#include "Agent.h"
#include "Vector2D.h"
#include <vector>

class Agent;

class SteeringBehavior
{
public:
	SteeringBehavior();
	~SteeringBehavior();
	Vector2D KinematicSeek(Agent *agent, Vector2D target, float dtime);
	Vector2D KinematicSeek(Agent *agent, Agent *target, float dtime);
	Vector2D KinematicFlee(Agent *agent, Vector2D target, float dtime);
	Vector2D KinematicFlee(Agent *agent, Agent *target, float dtime);

	/* Add here your own Steering Behavior functions declarations */
	Vector2D Seek(Agent *agent, Vector2D target, float factor, float dtime);
	Vector2D Seek(Agent *agent, Agent *target, float factor, float dtime);
	Vector2D Flee(Agent *agent, Vector2D target, float factor, float dtime);
	Vector2D Flee(Agent *agent, Agent *target, float factor, float dtime);

	Vector2D Arrive(Agent *agent, Vector2D target, float slowingRadius, float dtime);
	Vector2D Arrive(Agent *agent, Agent *target, float slowingRadius, float dtime);

	Vector2D Pursue(Agent *agent, Agent *target, float dtime);
	Vector2D Evade(Agent *agent, Agent *target, float factor, float dtime);

	Vector2D Wander(Agent *agent, float wanderOffset, float wanderRadius, float wanderMaxChange, float dtime, Vector2D &target);
	Vector2D OwnWander(Agent *agent, float wanderOffset, float wanderRadius, float wanderMaxChange, float wanderMaxChangeOffset, float dtime, Vector2D &target);
	
	Vector2D FlocSeparation(std::vector<Agent*> agents, Agent* agent, float NEIGHBOR_RADIUS);
	Vector2D FlocCohesion(std::vector<Agent*> agents, Agent* agent, float NEIGHBOR_RADIUS);
	Vector2D FlocAlignment(std::vector<Agent*> agents, Agent* agent, float NEIGHBOR_RADIUS);

	Vector2D PerimeterAvoidance(Agent* agent, float margin, float wWidht, float wHeight);

	Vector2D CollsionAvoidance(Agent* agent, std::vector<Agent*> entities, float coneAngle, float coneLength, float dtime);
	//etc...

};
